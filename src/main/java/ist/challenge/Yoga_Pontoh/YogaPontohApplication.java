package ist.challenge.Yoga_Pontoh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YogaPontohApplication {

	public static void main(String[] args) {
		SpringApplication.run(YogaPontohApplication.class, args);
	}

}
