package ist.challenge.Yoga_Pontoh.controller;

import ist.challenge.Yoga_Pontoh.entityss.AppUserEntity;
import ist.challenge.Yoga_Pontoh.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(value = "CheckAPI")
    public String checkAPI() {
        return "Server Nyala";
    }

    @PostMapping(value = "addUser")
    public ResponseEntity<String> addUser(@RequestBody AppUserEntity value) {
        try {
            userService.addUser(value);
            return new ResponseEntity<>("Success add new user", HttpStatus.CREATED);
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>("Username sudah terpakai", HttpStatus.CONFLICT);
        }
    }

    @PostMapping(value = "login")
    public ResponseEntity<String> login(@RequestBody AppUserEntity value) {
        if (value.getUsername() == null || value.getUsername().isEmpty() || value.getPassword() == null || value.getPassword().isEmpty()) {
            return new ResponseEntity<>("Username dan / atau password kosong", HttpStatus.BAD_REQUEST);
        }
        if (userService.login(value) != null && userService.login(value).equals("Sukses")) {
            return new ResponseEntity<>("Sukses Login", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Username dan / atau password salah", HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(value = "listUsers")
    public List<AppUserEntity> listUsers() {
        return userService.listUsers();
    }

    @PutMapping(value = "updateUser")
    public ResponseEntity<String> updateUser(@RequestBody AppUserEntity value) {
        try {
            return userService.updateUser(value);
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>("Username sudah terpakai", HttpStatus.CONFLICT);
        }
    }
}
