package ist.challenge.Yoga_Pontoh.service;

import ist.challenge.Yoga_Pontoh.entityss.AppUserEntity;
import ist.challenge.Yoga_Pontoh.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void addUser(AppUserEntity value) {
        userRepository.save(value);
    }

    public String login(AppUserEntity value) {
        Optional<AppUserEntity> user = userRepository.findByUsername(value.getUsername());
        if (user.isPresent()) {
            if (user.get().getPassword().equals(value.getPassword())) {
                return "Sukses";
            }
        }
        return "Gagal";
    }

    public List<AppUserEntity> listUsers () {
        return userRepository.findAll();
    }

    public ResponseEntity<String> updateUser(AppUserEntity value) {
        Optional<AppUserEntity> user = userRepository.findById(value.getId());
        if (user.isPresent()) {
            if (user.get().getPassword().equals(value.getPassword())) {
                return new ResponseEntity<>("Password tidak boleh sama dengan password sebelumnya", HttpStatus.BAD_REQUEST);
            }
            userRepository.save(value);
            return new ResponseEntity<>("Sukses", HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Data tidak ditemukan", HttpStatus.BAD_REQUEST);
    }
}
