package ist.challenge.Yoga_Pontoh.controller;

import ist.challenge.Yoga_Pontoh.entityss.AppUserEntity;
import ist.challenge.Yoga_Pontoh.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class UserControllerTest {
    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    AppUserEntity user = new AppUserEntity();

    @Test
    void addUserSuccessTest(){
        user.setUsername("Testusername");
        user.setPassword("testpassword");

        ResponseEntity<String> responseEntity = userController.addUser(user);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals("Success add new user", responseEntity.getBody());
    }

    @Test
    void addUserDuplicateTest(){
        user.setUsername("Dolos");
        user.setPassword("124");

        doThrow(DataIntegrityViolationException.class).when(userService).addUser(user);

        ResponseEntity<String> responseEntity = userController.addUser(user);
        assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
        assertEquals("Username sudah terpakai", responseEntity.getBody());
    }

    @Test
    void loginNullTest() {
        user.setUsername(null);
        user.setPassword(null);

        ResponseEntity<String> responseEntity = userController.login(user);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals("Username dan / atau password kosong", responseEntity.getBody());
    }

    @Test
    void loginEmptyTest() {
        user.setUsername("");
        user.setPassword("");

        ResponseEntity<String> responseEntity = userController.login(user);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals("Username dan / atau password kosong", responseEntity.getBody());
    }

    @Test
    void loginSuksesTest() {
        user.setUsername("Dolos");
        user.setPassword("124");

        when(userService.login(user)).thenReturn("Sukses");

        ResponseEntity<String> responseEntity = userController.login(user);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("Sukses Login", responseEntity.getBody());
    }

    @Test
    void loginInvalidTest() {
        user.setUsername("Dolosssd");
        user.setPassword("124d");

        when(userService.login(user)).thenReturn("gagal");

        ResponseEntity<String> responseEntity = userController.login(user);

        assertEquals(HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
        assertEquals("Username dan / atau password salah", responseEntity.getBody());
    }

    @Test
    void updateUserSuccessTest() {
        user.setId(1L);
        user.setUsername("test");
        user.setPassword("test");

        when(userService.updateUser(user)).thenReturn(new ResponseEntity<>("Sukses", HttpStatus.CREATED));

        ResponseEntity<String> responseEntity = userController.updateUser(user);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals("Sukses", responseEntity.getBody());
    }

    @Test
    void updateUserDuplicateUsernameTest() {
        user.setId(1L);
        user.setUsername("test");
        user.setPassword("test");

        doThrow(DataIntegrityViolationException.class).when(userService).updateUser(user);

        ResponseEntity<String> responseEntity = userController.updateUser(user);

        assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
        assertEquals("Username sudah terpakai", responseEntity.getBody());
    }

    @Test
    void updateUserDuplicatePasswordTest() {
        user.setId(1L);
        user.setUsername("test");
        user.setPassword("test");

        when(userService.updateUser(user)).thenReturn(new ResponseEntity<>("Password tidak boleh sama dengan password sebelumnya", HttpStatus.BAD_REQUEST));

        ResponseEntity<String> responseEntity = userController.updateUser(user);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals("Password tidak boleh sama dengan password sebelumnya", responseEntity.getBody());

    }

    @Test
    void updateUserNotFound() {
        user.setId(999999999L);
        user.setUsername("test");
        user.setPassword("test");

        when(userService.updateUser(user)).thenReturn(new ResponseEntity<>("Data tidak ditemukan", HttpStatus.BAD_REQUEST));

        ResponseEntity<String> responseEntity = userController.updateUser(user);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals("Data tidak ditemukan", responseEntity.getBody());
    }
}